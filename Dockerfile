FROM node:7-alpine
MAINTAINER "Live platform Team" <hw-live@thoughtworks.com>

RUN apk --update add git supervisor && rm -rf /var/cached/apk/*

# Prepare work directory and copy all files
RUN mkdir /app
WORKDIR /app

ADD package.json .
RUN npm i --silent

ADD supervisord.conf .
ADD watcher-tasks.js .
ADD template ./template
ADD resources ./resources

EXPOSE 8080 8081
CMD ["/usr/bin/supervisord"]
